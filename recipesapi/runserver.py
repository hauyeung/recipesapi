"""
This script runs the recipesapi application using a development server.
"""

from os import environ
from recipesapi import app

if __name__ == '__main__':
    app.config.update(dict(
        DATABASE = '/recipesdb.sqlite3',
        DEBUG = True,
        SECRET_KEY = 'development key',
        USERNAME = 'admin',
        PASSWORD = 'default'
        ))
    app.config.from_envvar('FLASKR_SETTINGS', silent=True)        
    HOST = environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app.run(HOST, PORT)
