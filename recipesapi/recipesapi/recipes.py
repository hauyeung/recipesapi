"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template, abort, Blueprint, redirect, request, url_for, send_from_directory
from recipesapi import app
from recipesdb import *
from werkzeug import secure_filename
import os

UPLOAD_FOLDER = 'images'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
PATH = u'c:\\users\\ho hang\\documents\\visual studio 2013\\Projects\\recipesapi\\recipesapi\\images'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

recipespage = Blueprint('recipespage',__name__, url_prefix='/recipes')
@recipespage.route('/')
@recipespage.route('/home')
def recipehome():    
    db = get_db()
    cur = db.execute('select * from recipes')
    recipes = cur.fetchall()
    return render_template(
        'recipelist.html',
        title='Recipes',
        year=datetime.now().year,
        recipes = recipes
    )

@recipespage.route('/addrecipeform')
def addrecipeform():    
    return render_template(
        'addrecipe.html',
        title='Add Recipe',
        year=datetime.now().year,
    )

@recipespage.route('/addrecipe', methods=['POST'])
def addrecipe():    
    title = request.form['title']
    text = request.form['text']
    file = request.files['image']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    db = get_db()
    db.execute('insert into recipes (title, text, imageurl, createdate) values (?,?,?, ?)',(title, text,filename, datetime.now()))
    db.commit()
    return redirect(url_for('recipespage.recipehome'))

@recipespage.route('/editrecipeform/<recipeid>')
def editrecipeform(recipeid=None):    
    return render_template(
        'editrecipe.html',
        title='Edit Recipe',
        year=datetime.now().year,
        recipeid = recipeid
    )

@recipespage.route('/editrecipe', methods=['POST'])
def editrecipe():    
    title = request.form['title']
    text = request.form['text']
    recipeid = request.form['recipeid']
    file = request.files['image']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    db = get_db()
    db.execute('update recipes set title=?, text=?, imageurl=? where id=?',(title, text,filename, recipeid))
    db.commit()
    return redirect(url_for('recipespage.recipehome'))

@recipespage.route('/deleterecipe/<recipeid>')
def deleterecipe(recipeid=None): 
    db = get_db()
    db.execute('delete from recipes where id=?', recipeid)
    db.commit()   
    return redirect(url_for('recipespage.recipehome'))

@recipespage.route('/uploads/<filename>')
def getuploadedfile(filename):    
    return send_from_directory(PATH,
                               filename)

