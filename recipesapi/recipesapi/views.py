"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, Blueprint
from recipesapi import app
import md5
from sqlite3 import dbapi2 as sqlite3
from recipesdb import *
import hashlib


userpage = Blueprint('userpage',__name__)
@userpage.route('/')
@userpage.route('/home')
def home():
    db = get_db()
    cur = db.execute('select * from recipes limit 10')
    recipes = cur.fetchall()
    db = get_db()
    cur = db.execute('select * from users limit 10')
    users = cur.fetchall()
    return render_template('index.html', title='Home', year=datetime.now().year, users = users, recipes = recipes)


@userpage.route('/userslist')
def userhome():    
    db = get_db()
    cur = db.execute('select * from users')
    users = cur.fetchall()
    return render_template(
        'userlist.html',
        title='Users List',
        year=datetime.now().year,
        users = users
    )

@userpage.route('/adduserform')
def adduserform():    
    return render_template(
        'adduser.html',
        title='Add User',
        year=datetime.now().year,
    )

@userpage.route('/adduser', methods=['POST'])
def adduser():    
    username = request.form['username']
    password = request.form['password']
    confirmpassword = request.form['confirmpassword']
    
    if password == confirmpassword:               
        db = get_db()
        db.execute('insert into users (username, password) values (?,?)',(username, hashlib.sha256(password).hexdigest()))
        db.commit()
    return redirect(url_for('userpage.userhome'))

@userpage.route('/edituserform/<userid>')
def edituserform(userid=None):    
    return render_template(
        'edituser.html',
        title='Edit User',
        year=datetime.now().year,
        userid = userid
    )

@userpage.route('/edituser', methods=['POST'])
def edituser():    
    username = request.form['username']
    password = request.form['password']
    confirmpassword = request.form['confirmpassword']
    userid = request.form['userid']
    if password == confirmpassword:
        db = get_db()
        m = hashlib.md5()
        m.update(password)
        hashedpassword = m.digest()
        db.execute('update users set username=?,password=? where id=?',(username, hashlib.sha256(password).hexdigest(), userid))
        db.commit()
    return redirect(url_for('userpage.userhome'))

@userpage.route('/deleteuser/<userid>')
def deleteuser(userid=None):    
    db = get_db()
    db.execute('delete from users where id=?', userid)
    db.commit()
    return redirect(url_for('userpage.userhome'))