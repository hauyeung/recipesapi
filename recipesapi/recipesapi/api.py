"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template, abort, Blueprint, jsonify
from recipesapi import app
import json
from recipesdb import get_db
from flask.ext.httpauth import HTTPBasicAuth, HTTPDigestAuth
import hashlib
import md5
app.config['SECRET_KEY'] = 'secret'
auth = HTTPBasicAuth()

@auth.verify_password
def verify_pw(username, password):
    db = get_db()
    cur = db.execute('select * from users where username=? and password=?', (username, hashlib.sha256(password).hexdigest())) 
    if cur.fetchone() != None:
        return True
    else:
        return False


recipesapi = Blueprint('recipesapi',__name__, url_prefix='/api')
@recipesapi.route('/')
@recipesapi.route('/home')
@auth.login_required
def recipeapihome():    
    db = get_db()
    cur = db.execute('select * from recipes')
    recipes = cur.fetchall()       
    recipe = dict()
    recipeslist = []
    for r in recipes:
        recipe = {'title' : r[1], 'text': r[2], 'imageurl': r[3]}
        recipeslist.append(recipe)        
    return json.dumps(recipeslist)

@recipesapi.route('/<recipeid>')
@auth.login_required
def getrecipebyid(recipeid=None):    
    db = get_db()
    cur = db.execute('select * from recipes where id=?', recipeid)
    result = cur.fetchone()       
    if result != None:
        recipe = dict()    
        recipe = {'title' : result[1], 'text': result[2], 'imageurl': result[3]}    
        return json.dumps(recipe)
    else:
        return 'No result found'


