from sqlite3 import dbapi2 as sqlite3
from recipesapi import app
from flask import g

def connect_db():    
    rv = sqlite3.connect('recipesdb.sqlite3')
    rv.row_factory = sqlite3.Row
    return rv


def init_db():    
    db = get_db()
    with app.open_resource('recipesdb.sqlite3', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

def get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db
