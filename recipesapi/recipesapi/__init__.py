"""
The flask application package.
"""

from flask import Flask, Blueprint
app = Flask(__name__)

from views import userpage
from recipes import recipespage
from api import recipesapi
app.register_blueprint(userpage)
app.register_blueprint(recipespage)
app.register_blueprint(recipesapi)

